import numpy as np
import cooler
import bioframe
import pandas as pd
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-i', help='input cooler', type=str, dest='i')
parser.add_argument('-o', help='output file', type=str, dest='o')
args = parser.parse_args()

chromsizes = bioframe.fetch_chromsizes('mm9')
chroms = list(chromsizes.index)
c = cooler.Cooler(args.i)

bins = bioframe.tools.binnify(chromsizes, binsize=50000)  # 50kb bins
bins['cis'] = 0
bins['trans'] = 0
for chrom in chroms:
    bins['trans-%s'%chrom] = 0
  
for chrom1 in ['chr7', 'chr18']:
    for chrom2 in chroms:
        print(chrom1, chrom2)
        m = c.matrix(balance=False).fetch(chrom1, chrom2)
        #m[m > 0] = 1
        ids = bioframe.slice_bedframe(bins, chrom1).index
        if chrom1 == chrom2:
            # cis calculation
            cis = np.sum(m, axis=1)
            bins.loc[ids, 'cis'] = cis
            bins.loc[ids, 'trans-{}'.format(chrom2)] = cis
        else:
            # trans calculation
            trans = np.sum(m, axis=1)
            bins.loc[ids, 'trans'] += trans
            bins.loc[ids, 'trans-{}'.format(chrom2)] = trans
bins.to_csv(args.o, index=False, sep='\t')
